﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;
using static Internal<VariableName, double>;
using static Internal<VariableName, double>.Branch<VariableName, double>;
using static Internal<VariableName, double>.OperatorNumber;
using NumberBranch = Internal<VariableName, double>.Branch<VariableName, double>;
using NumberVariable = Internal<VariableName, double>.Operand<double>.Variable<VariableName>;
public class Cell : MonoBehaviour
{
	[SerializeField] TextMeshPro text;
	[SerializeField] LineRenderer line;
	static GameObject prefab
	{
		get
		{
			return Resources.Load("cell_prefab") as GameObject;
		}
	}
	public void Init(NumberBranch branch, int depth = 0, Cell parental = null)
	{
		float dDISTANCE = 2f;
		float dUP = 2;
		float dSCALE = 0.8f;

		if(parental != null)
		{
			line.SetPosition(0, Vector3.zero);
			line.SetPosition(1, -Vector3.forward / transform.localScale.magnitude * dDISTANCE / dSCALE  + Vector3.up*dUP);
		}
		else if(depth == 0)
		{
			if (parental != null)
			{
				line.SetPosition(0, Vector3.zero);
				line.SetPosition(1, parental.transform.position - transform.position);
			}
			else
			{
				line.enabled = false;
			}
		}

		var branching   = branch as NumberBranch.Branching;
		var terminating = branch as NumberBranch.Terminating;
		if(branching != null)
		{
			Cell child;


			Transform left = (Instantiate(prefab) as GameObject).transform;
			left.SetParent(transform);
			left.localScale = dSCALE * Vector3.one;
			left.localPosition = Vector3.left * dDISTANCE - Vector3.up * dUP;
			left.forward = -transform.right;
			child = left.GetComponent<Cell>();
			child.Init(branching.left, depth + 1, this);

			Transform right = (Instantiate(prefab) as GameObject).transform;
			right.SetParent(transform);
			right.localScale = dSCALE * Vector3.one;
			right.localPosition = Vector3.right * dDISTANCE - Vector3.up * dUP;
			child = right.GetComponent<Cell>();
			right.forward = transform.right;
			child.Init(branching.right, depth + 1, this);

			text.text = branching.o.Representation();
			text.color = Color.white;
		}
		else
		{
			text.text = terminating.Representation();
			if(terminating.data is NumberVariable)
			{
				text.color = Color.cyan;
			}
			else
			{
				text.color = Color.green;
			}
		}




	}

	public static Cell Factory(NumberBranch root)
	{
		var ret = GameObject.Instantiate(prefab).GetComponent<Cell>();
		ret.Init(root);
		return ret;
	}
	public static Cell Factory(NumberBranch root, int n, int nMax, int generation)
	{
		Cell c = Factory(root);
		float theta = (n / (float)nMax + Mathf.Deg2Rad * 360f * generation / 10) * 2f * Mathf.PI;
		c.transform.position = 
			10 * (
			Vector3.right * Mathf.Sin(theta) +
			Vector3.forward * Mathf.Cos(theta)
			) +
			0 * Vector3.up * generation * 5;
		return c;
	}
}