﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using RNG = UnityEngine.Random;

public class Internal<Tki, Tvi> {
	public static List<Branch<Tki, Tvi>> Propagate(List<Branch<Tki, Tvi>> seeds)
	{
		List<Branch<Tki, Tvi>> propagations = new List<Branch<Tki, Tvi>>();
		for (int i = 0; i < seeds.Count; i++)
		{
			var si = seeds[i];
			for (int j = i + 1; j < seeds.Count; j++)
			{
				var sj = seeds[j];
				var cp = si.CrossPolinate(sj, 4);
				if (cp.Count == 0)
					propagations.Add(si);
				else
					propagations.AddRange(cp);
			}
		}
		return propagations;
	}
	public static double Judge(Branch<Tki, Tvi> seed, Func<Dictionary<Tki, Tvi>, Tvi> ftarget, params Dictionary<Tki, Tvi>[] mods)
	{
		List<double> deltas;
		return Judge(seed, ftarget, out deltas, mods);
	}
	public static double Judge(Branch<Tki, Tvi> seed, Func<Dictionary<Tki, Tvi>, Tvi> ftarget, out List<double> deltas, params Dictionary<Tki, Tvi>[] mods)
	{
		double dSum = 0;
		deltas = new List<double>();
		foreach (Dictionary<Tki, Tvi> mod in mods)
		{
			double d = DeltaSquared(
				ftarget(mod),
				seed.Evaluate(mod).Value()
				);
			deltas.Add(d);
			dSum += d;
		}
		return dSum / mods.Count();
	}
	public static List<Branch<Tki, Tvi>> TakeBest(List<Branch<Tki, Tvi>> seeds, int take, Func<Dictionary<Tki, Tvi>, Tvi> ftarget, params Dictionary<Tki, Tvi>[] mods)
	{
		List<KeyValuePair<Branch<Tki, Tvi>, double>> ret = new List<KeyValuePair<Branch<Tki, Tvi>, double>>();

		foreach (Branch<Tki, Tvi> seed in seeds)
		{
			double dSum = Judge(seed, ftarget, mods);
			ret.Add(new KeyValuePair<Branch<Tki, Tvi>, double>(seed, dSum));
		}
		return TakeBest(ret, take);
	}
	public static List<Branch<Tki, Tvi>> TakeBest(List<KeyValuePair<Branch<Tki, Tvi>, double>> seeds, int take)
	{
		return seeds
			.GroupBy(e => e.Key.Representation())
			.Select(e => e.FirstOrDefault())
			.OrderBy(e => Math.Abs(e.Value))
			.Select(e => e.Key)
			.Take(take)
			.ToList();
	}
	public static Branch<Tki, Tvi> Consolidate(Branch<Tki, Tvi> branch)
	{
		var branching = branch as Branch<Tki, Tvi>.Branching;
		if (branching != null)
		{	
			//check all are primitive, if so, consolidate them!
			{
				bool allArePrimitive = true;
				List<Operand<Tvi>.Primitive> primitives = new List<Operand<Tvi>.Primitive>();
				foreach (Branch<Tki, Tvi> e in branching.branches)
				{
					Branch<Tki, Tvi>.Terminating asTerminating = e as Branch<Tki, Tvi>.Terminating;
					if (asTerminating != null)
					{
						Operand<Tvi>.Primitive primitive = asTerminating.data as Operand<Tvi>.Primitive;
						if (primitive != null)
						{
							primitives.Add(primitive);
							continue;
						}
					}
					allArePrimitive = false;
					break;
				}
				if (allArePrimitive)
				{
					
					//else
					{
						Branch<Tki, Tvi> left = branching.left;
						Branch<Tki, Tvi> right = branching.right;
						Tvi v = branching.Evaluate().Value();
						Tvi v_left = left .Evaluate().Value();
						Tvi v_right= right.Evaluate().Value();
						if(v.Equals(v_left))
						{
							return new Branch<Tki,Tvi>.Terminating(v_left);
						}
						else if(v.Equals(v_right))
						{
							return new Branch<Tki,Tvi>.Terminating(v_right);
						}
						else
						{
							if (RNG.value < 0.5f)
							{
								var ret = new Branch<Tki, Tvi>.Terminating(branch.o.Operate(primitives[0], primitives[1]));
								return ret;
							}
						}
					}
				}
			}


			{
				for(int i = 0; i < branching.branches.Count(); i++)
				{
					branching.branches[i] = Consolidate(branching.branches[i]);
				}
			}
		}
		return branch;
	}

	public static double DeltaSquared(Tvi a_, Tvi b_)
	{
		double a = (double)Convert.ChangeType(a_, typeof(double));
		double b = (double)Convert.ChangeType(b_, typeof(double));
		return DeltaSquared(a, b);
	}
	public static double DeltaSquared(Tvi a_, double b)
	{
		double a = (double)Convert.ChangeType(a_, typeof(double));
		return DeltaSquared(a, b);
	}
	public static double DeltaSquared(double a, double b)
	{
		if (Double.IsNaN(a) || Double.IsNaN(b)) return Double.MaxValue;
		return (a - b) * (a - b);
	}


	public abstract class Operand<T> : IRepresentable
	{
		private T data;

		public abstract T Value();
		public abstract string Representation();

		public class Primitive : Operand<T>
		{
			public Primitive(T data)
			{ this.data = data; }
			public override T Value()
			{
				return data;
			}
			public override string Representation()
			{
				return String.Format("{0}", Value());
			}
		}

		public class Variable<Tk> : Operand<T>
		{
			private readonly Tk key;
			public Variable(Tk key)
			{
				this.key = key;
			}
			Dictionary<Tk, T> variables = null;
			public override T Value() { return variables[key]; }
			public T SetVariables(Dictionary<Tk,T> variables = null)
			{
				if(variables != null && variables.ContainsKey(key))
				{
					this.variables = variables;
					return variables[key];
				}
				return default(T);
			}
			public override string Representation()
			{
				//return String.Format("<k{0}:v{1}>", key, Value());
				return String.Format("{0}", key) ;
			}
		}
	}
	public abstract class Branch<Tk, Tv> : IRepresentable
	{
		private List<Branch<Tk, Tv>> parents;
		public Branch(params Branch<Tk, Tv>[] parents)
		{
			this.parents = new List<Branch<Tk, Tv>>(parents);
		}



		public Operator<Tv> o;

		public abstract Operand<Tv> Evaluate(Dictionary<Tk, Tv> variables = null);
		public delegate Branch<Tk, Tv> MutationDelegate(Branch<Tk, Tv> branch);
		public abstract List<Branch<Tk, Tv>> CrossPolinate(Branch<Tk, Tv> other, int limit = -1);
		public abstract Branch<Tk, Tv> Clone();
		public virtual String Representation()
		{
			return "<unknown>";
		}
		public virtual List<Branch<Tk, Tv>> Mutate(List<MutationDelegate> mutationsTerminating, List<MutationDelegate> mutationsBranching, int nTries = 1)
		{
			List<Branch<Tk, Tv>> mutations = new List<Branch<Tk, Tv>>();
			for(int i = 0; i < nTries; i++)
			{
				foreach(MutationDelegate md in mutationsTerminating)
				{
					Branch<Tk, Tv> mutated = MutateSingle(md, this);
					if (mutated == null) continue;
					mutations.Add(mutated);
				}
				foreach (MutationDelegate md in mutationsBranching)
				{
					if (!(this is Branching)) continue;
					var mutated = md.Invoke(this) as Branching;
					mutations.Add(mutated);
				}
			}
			return mutations.FindAll(m => m != null);
		}
		public static Branch<Tk, Tv> MutateSingle(MutationDelegate mutationDel, Branch<Tk, Tv> branch)
		{
			if (branch is Terminating)
			{
				return mutationDel(branch);
			}
			else if (branch is Branching)
			{
				Branch<Tk,Tv> clone = branch.Clone();
				var branches = (clone as Branching).branches;
				for (int i = 0; i < branches.Length; i++)
				{
					branches[i] = MutateSingle(mutationDel, branches[i]);
				}
				return clone;
			}
			return null;
		}


		public class Branching : Branch<Tk, Tv>
		{
			public readonly Branch<Tk, Tv>[] branches;
			public Branch<Tk, Tv> left { get { return branches[0]; } }
			public Branch<Tk, Tv> right{ get { return branches[branches.Count()-1]; } }
			public Branching(Branch<Tk, Tv>[] branches, Operator<Tv> o, params Branch<Tk, Tv>[] parents) : base(parents)
			{
				this.o = o;
				this.branches = branches;
			}
			public Branching(Operator<Tv> o, params Branch<Tk, Tv>[] branches)
			{
				this.o = o;
				this.branches = branches;
			}
			public override Operand<Tv> Evaluate(Dictionary<Tk, Tv> variables = null)
			{
				Operand<Tv>[] operands = new Operand<Tv>[branches.Length];
				operands[0] = left.Evaluate(variables);
				operands[1] = right.Evaluate(variables);
				Operand<Tv> data = o.Operate(operands[0], operands[1]);
				return data;
			}
			public override Branch<Tk, Tv> Clone()
			{
				Branch<Tk, Tv>[] _branches = new Branch<Tk, Tv>[branches.Length];
				for(int i = 0; i < branches.Length; i++)
				{
					_branches[i] = branches[i];
				}
				var _o = o.Clone();
				return new Branching(_branches, _o);
			}
			public override List<Branch<Tk, Tv>> CrossPolinate(Branch<Tk, Tv> other, int limit = -1)
			{
				if(other is Branching)
				{
					return CrossPolinate(other as Branching, limit);
				}
				return default(List<Branch<Tk, Tv>>);
			}
			public List<Branch<Tk, Tv>> CrossPolinate(Branching other, int limit = -1)
			{
				List<Branch<Tk, Tv>> crossPolinations = new List<Branch<Tk, Tv>>();
				List<Operator<Tv>> operators = new List<Operator<Tv>>();
				operators.Add(this.o); operators.Add(other.o);
				foreach(Operator<Tv> op in operators)
				{
					crossPolinations.Add(new Branching(
						new Branch<Tk, Tv>[]
						{
							left.Clone(), other.left.Clone()
						},
						op.Clone()
					));
					crossPolinations.Add(new Branching(
						new Branch<Tk, Tv>[]
						{
							left.Clone(), other.right.Clone()
						},
						op.Clone()
					));
					crossPolinations.Add(new Branching(
						new Branch<Tk, Tv>[]
						{
							right.Clone(), other.left.Clone()
						},
						op.Clone()
					));
					crossPolinations.Add(new Branching(
						new Branch<Tk, Tv>[]
						{
							right.Clone(), other.right.Clone()
						},
						op.Clone()
					));
					crossPolinations.Add(new Branching(
						new Branch<Tk, Tv>[]
						{
							Clone(), other.Clone()
						},
						op.Clone()
					));
					crossPolinations.Add(new Branching(
						new Branch<Tk, Tv>[]
						{
							other.Clone(), Clone()
						},
						op.Clone()
					));
					crossPolinations.Add(new Branching(
						new Branch<Tk, Tv>[]
						{
							Clone(), other.left.Clone()
						},
						op.Clone()
					));
					crossPolinations.Add(new Branching(
						new Branch<Tk, Tv>[]
						{
							other.right.Clone(), Clone()
						},
						op.Clone()
					));
				}
				if (limit > 0)
					return crossPolinations.Take(limit).ToList();
				return crossPolinations;
			}
			public override String Representation()
			{
				//RepresentationCounter++;
				return String.Format("{3} {0}{2}{1} {4}", left.Representation(), right.Representation(), o.Representation(),
					RepresentationCounter % 2 == 0 ? "(" : "[",
					RepresentationCounter % 2 == 0 ? ")" : "]"
					);
			}
			private static int RepresentationCounter = 0;
		}
		public class Terminating : Branch<Tk, Tv>
		{
			public Terminating(Tv data) { this.data = new Operand<Tv>.Primitive(data); }
			public Terminating(Operand<Tv> data, params Branch<Tk, Tv>[] parents) : base(parents) { this.data = data; }
			public readonly Operand<Tv> data;
			public override Operand<Tv> Evaluate(Dictionary<Tk, Tv> variables = null)
			{
				if (data is Operand<Tv>.Primitive) return data;
				if (data is Operand<Tv>.Variable<Tk>)
				{
					(data as Operand<Tv>.Variable<Tk>).SetVariables(variables);
					return data;
				}
				return null;
			}
			public override Branch<Tk, Tv> Clone()
			{
				return new Terminating(data);
			}
			public override List<Branch<Tk, Tv>> CrossPolinate(Branch<Tk, Tv> other, int limit = -1)
			{
				if(other is Terminating)
				{
					return CrossPolinate(other as Terminating, limit);
				}
				return default(List<Branch<Tk,Tv>>);
			}
			private List<Branch<Tk, Tv>> CrossPolinate(Terminating other, int limit = -1)
			{ 
				List<Branch<Tk, Tv>> crossPolinations = new List<Branch<Tk, Tv>>();
				//crossPolinations.Add(new Terminating(data.Value()));
				//crossPolinations.Add(new Terminating(other.data.Value()));
				return crossPolinations;
			}
			public override String Representation()
			{
				return String.Format("{0}", data.Representation());
			}
		}


	}


	public abstract class Operator<Tv> : IRepresentable
	{
		public abstract Operand<Tv> Operate(Operand<Tv> o1, Operand<Tv> o2);
		public abstract Operator<Tv> Clone();

		public abstract Operator<Tv> Mutate();

		public abstract string Representation();
	}

	public abstract class OperatorNumber : Operator<double>
	{
		public override Operand<double> Operate(Operand<double> o1, Operand<double> o2)
		{
			return new Operand<double>.Primitive(
				Operate(o1.Value(), o2.Value())
				);
		}
		public abstract double Operate(double o1, double o2);
		public override Operator<double> Clone()
		{
			return this;
		}
		protected abstract OperatorNumber MutateInternal();
		public override Operator<double> Mutate()
		{
			return MutateInternal();
		}

		public class Plus : OperatorNumber
		{
			public override double Operate(double o1, double o2)
			{
				return o1 + o2;
			}
			protected override OperatorNumber MutateInternal()
			{
				return (RNG.value < 0.5f) ? (OperatorNumber)new Minus() : (OperatorNumber)new Multiply();
			}
			public override string Representation(){return "+";}
		}
		public class Minus : OperatorNumber
		{
			public override double Operate(double o1, double o2)
			{
				return o1 - o2;
			}
			protected override OperatorNumber MutateInternal()
			{
				return (RNG.value < 0.5f) ? (OperatorNumber)new Plus() : (OperatorNumber)new Divide();
			}
			public override string Representation() { return "-"; }
		}
		public class Multiply : OperatorNumber
		{
			public override double Operate(double o1, double o2)
			{
				return o1 * o2;
			}
			protected override OperatorNumber MutateInternal()
			{
				return (RNG.value < 0.8f) ? (OperatorNumber)new Plus() : (OperatorNumber)new Power();
			}
			public override string Representation() { return "*"; }
		}
		public class Divide : OperatorNumber
		{
			public override double Operate(double o1, double o2)
			{
				return o2 == 0 ? double.MaxValue : o1 / o2;
			}
			protected override OperatorNumber MutateInternal()
			{
				return (RNG.value < 0.8f) ? (OperatorNumber)new Minus() : (OperatorNumber)new Root();
			}
			public override string Representation() { return "/"; }
		}
		public class Power : OperatorNumber
		{
			public override double Operate(double o1, double o2)
			{
				return Math.Pow(o1, o2);
			}
			protected override OperatorNumber MutateInternal()
			{
				return (RNG.value < 0.5f) ? (OperatorNumber)new Plus() : (OperatorNumber)new Multiply();
			}
			public override string Representation() { return "^"; }
		}
		public class Root : OperatorNumber
		{
			public override double Operate(double o1, double o2)
			{
				return Math.Pow(o1, 1f/Math.Max(0.0001f,o2));
			}
			protected override OperatorNumber MutateInternal()
			{
				return (RNG.value < 0.5f) ? (OperatorNumber)new Minus() : (OperatorNumber)new Divide();
			}
			public override string Representation() { return "√¯"; }
		}
	}



	public interface IRepresentable
	{
		String Representation();
	}



}
