﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using static Internal<VariableName, double>;
using static Internal<VariableName, double>.Branch<VariableName, double>;
using static Internal<VariableName, double>.OperatorNumber;
using NumberBranch = Internal<VariableName, double>.Branch<VariableName, double>;
using NumberVariable = Internal<VariableName, double>.Operand<double>.Variable<VariableName>;
using RNG = UnityEngine.Random;
using System.Text;



public enum VariableName { x, y, z }

public class Starter : MonoBehaviour {


	readonly List<VariableName> variables =
		new List<VariableName>{
			VariableName.x,
			VariableName.y,
		}
	;

	IEnumerator Start () {

		NumberBranch b;
		b = new Branching(
			new Plus(),
			new Terminating(1),
			new Terminating(2)
		);
		NumberBranch c;
		c = new Branching(new Plus(),
			new Terminating(5),
			new Branching(new Plus(),
				new Terminating(3),
				new Terminating(4)
			)
		);
		NumberBranch d;
		d = new Branching(new Plus(),
			new Terminating(1),
			new Terminating(new NumberVariable(VariableName.x))
		);
		NumberBranch e;
		e = new Branching(new Minus(),
			new Terminating(1),
			new Terminating(1)
		);


		List<VariableName> vns = new List<VariableName>(Enum.GetValues(typeof(VariableName)).Cast<VariableName>());
		List<NumberBranch.MutationDelegate> mutationsTerminating = new List<NumberBranch.MutationDelegate>()
		{
			delegate(NumberBranch original)
			{
				if(original is NumberBranch.Terminating)
				{
					double data = (original as NumberBranch.Terminating).data.Value();
					return new NumberBranch.Terminating(data - Mathf.Round(RNG.value*10)/10f);
				}
				return null;
			},
			delegate(NumberBranch original)
			{
				if(original is NumberBranch.Terminating)
				{
					double data = (original as NumberBranch.Terminating).data.Value();
					return new NumberBranch.Terminating(data + Mathf.Round(RNG.value*10)/10f);
				}
				return null;
			},
			delegate(NumberBranch original)
			{
				if(original is NumberBranch.Terminating)
				{
					return new NumberBranch.Terminating(0);
				}
				return null;
			},
			delegate(NumberBranch original)
			{
				if(original is NumberBranch.Terminating)
				{
					double data = (original as NumberBranch.Terminating).data.Value();
					return new NumberBranch.Terminating(data * -1f);
				}
				return null;
			},
			delegate(NumberBranch original)
			{
				if(original is NumberBranch.Terminating)
				{
					double data = (original as NumberBranch.Terminating).data.Value();
					return new NumberBranch.Branching(
						new Plus(),
						new Terminating(data),
						new Terminating(1)
					);
				}
				return null;
			},
			delegate(NumberBranch original)
			{
				if(original is NumberBranch.Terminating)
				{
					double data = (original as NumberBranch.Terminating).data.Value();
					return new NumberBranch.Branching(
						new Minus(),
						new Terminating(data),
						new Terminating(1)
					);
				}
				return null;
			},
			delegate(NumberBranch original)
			{
				if(original is NumberBranch.Terminating)
				{
					VariableName vn = vns[RNG.Range(0, vns.Count())];
					return new Terminating(new NumberVariable(vn));
				}
				return null;
			},
		};

		List<NumberBranch.MutationDelegate> mutationsBranching = new List<NumberBranch.MutationDelegate>()
		{
			delegate(NumberBranch original)
			{	//new random operator
				if(original is NumberBranch.Branching)
				{
					var ret = original.Clone();
					ret.o = ret.o.Mutate();
					return ret;
				}
				return null;
			},
			delegate(NumberBranch original)
			{	//branch swap
				if(original is NumberBranch.Branching)
				{
					NumberBranch.Branching ret = original.Clone() as NumberBranch.Branching;
					var t = ret.branches[0];
					ret.branches[0] = ret.branches[1];
					ret.branches[1] = t;
					return ret;
				}
				return null;
			},
			delegate(NumberBranch original)
			{	//add a + value to branch
				if(original is NumberBranch.Branching)
				{
					Branching ret = new Branching(
						new OperatorNumber.Plus(),
						original.Clone(),
						new Terminating(RNG.Range(0,10)/10f)
					);
					return ret;
				}
				return null;
			},
			delegate(NumberBranch original)
			{	//add a - value to branch
				if(original is Branching)
				{
					Branching ret = new Branching(
						new Minus(),
						original.Clone(),
						new Terminating(RNG.Range(0,10)/10f)
					);
					return ret;
				}
				return null;
			},
		};

		Func<Dictionary<VariableName, double>, double> TARGET = 
			delegate(Dictionary<VariableName, double> vars)
			{
				//return vars[VariableName.x] * 7 + 5 * 3 + Math.Pow(vars[VariableName.y], 2);
				//return (vars[VariableName.x] + 4) * vars[VariableName.y] + 5 + Math.Pow(vars[VariableName.z], 2);
				//return (vars[VariableName.x] - 24);
				return (vars[VariableName.x] * 2 + 4) * vars[VariableName.y] + vars[VariableName.z] * 3 ;
			}
		;


		var mods = new Dictionary<VariableName, double>[5];
		for(int i = 0; i < mods.Count(); i++)
		{
			Dictionary<VariableName, double> mod = new Dictionary<VariableName, double>();
			foreach (VariableName vn in Enum.GetValues(typeof(VariableName)))
			{
				mod.Add(vn, RNG.Range(-20, 20));
			}
			mods[i] = mod;
		}

		const int TAKE = 20; const int TAKELOG = 5;
		const int GENERATIONS = 100;
		List<NumberBranch> best = TakeBest(d.CrossPolinate(e), TAKE, TARGET, mods);
		NumberBranch champion = best[0];

		List<Cell> oldCells = new List<Cell>();
		List<Cell> cellGeneration = new List<Cell>();
		for (int generation = 0; generation < GENERATIONS; generation++)
		{
			StringBuilder log = new StringBuilder();

			log.AppendFormat("GENERATION #{0}\n", generation);

			double populationHealth = 0;
			foreach (var v in best)
			{
				populationHealth += Judge(v, TARGET, mods);
			}
			populationHealth = 100 - Mathf.Round(Mathf.Sqrt((float)populationHealth) / TAKE * 100f) / 100f;
			populationHealth = populationHealth < 0 ? 0 : populationHealth;

			champion = best.Take(1).ToList()[0];
			log.AppendFormat(" POPULATION HEALTH\t: {0}%\n", populationHealth);
			//log.AppendFormat(" TARGET HEALTH    \t: {0}\n", TARGET(variables));
			//log.AppendFormat(" DELTA            \t: {0}\n", Mathf.Abs((float)(TARGET(variables) - champion.Evaluate().Value())));

			oldCells.AddRange(cellGeneration);
			cellGeneration.Clear();

			for (int j = 0; j < TAKELOG; j++)
			{
				var v = best[j];
				List<double> deltas;
				Judge(v, TARGET, out deltas, mods);
				log.AppendFormat(" |({1})\t{0}\n", v.Representation(), Math.Round(deltas.Sum() / deltas.Count(), 2));

				cellGeneration.Add(Cell.Factory(v, j, TAKELOG, generation));
			}

			foreach(Cell oc in oldCells)
			{
				oc.transform.position = oc.transform.position.normalized * (oc.transform.position.magnitude + 10);
			}
			Debug.Log(log.ToString());

			List<NumberBranch> nBest = new List<NumberBranch>();

			for (int j = 0; j < best.Count; j++)
			{
				var branch = best[j];
				nBest.AddRange(TakeBest(branch.Mutate(mutationsTerminating, mutationsBranching, 5), 5, TARGET, mods));
			}
			nBest = nBest.Take(nBest.Count * 3 / 4).ToList();

			best = TakeBest(Propagate(nBest), TAKE, TARGET, mods).Select(s => Consolidate(s)).ToList();
			

			champion = best[0];

			while (!Input.GetKeyDown(KeyCode.Space)) yield return null;
			yield return null;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
